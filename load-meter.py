#!/usr/bin/env python
import serial
import time

devname = "enp3s0" # active network device
timestep = .125 # Seconds
port = serial.Serial('/dev/ttyUSB0', 57600) # serial port definition
old = None # compare variable
idleold = 0

def transmissionrate(dev, direction, timestep):
    """Return the transmisson rate of a interface under linux
    dev: devicename
    direction: rx (received) or tx (sended)
    timestep: time to measure in seconds
    """
    path = "/sys/class/net/{}/statistics/{}_bytes".format(dev, direction)
    f = open(path, "r")
    bytes_before = int(f.read())
    f.close()
    time.sleep(timestep)
    f = open(path, "r")
    bytes_after = int(f.read())
    f.close()
    return (bytes_after-bytes_before)/timestep

while True:
    with open('/proc/stat') as stat:
        new = map(float, stat.readline().strip().split()[1:])

    if old is not None:
        diff = [n - o for n, o in zip(new, old)]
        idle = diff[3] / sum(diff)
        netload = int(transmissionrate(devname, "rx", timestep)/10000)
        if netload > 255:
            netload = 255 # set max value to 255 if bigger

        values = bytearray([(int(255 * (1 - idle))), (netload)]) # create the send array
        port.write(values)


    old = new
    time.sleep(0.125)
