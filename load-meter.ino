/*
 * This is the arduino code of the source meter.
 */
 
#define CPU 3         //arduino pin 3 drives the analog panel for cpu load
#define NETWORK 5     //arduino pin 5 drives the LED for network load

int incoming[2];      //incoming array variable for the data (2 fields a.t.m.)

void setup() {
  Serial.begin(57600);    //initialize serial port
  pinMode(CPU, OUTPUT);   //initialize pin3 as output
  pinMode(NETWORK, OUTPUT); //initialize pin5 as output
}

void loop() {
  while(Serial.available() >= 2){
    // fill array
    for (int i = 0; i < 2; i++){
      incoming[i] = Serial.read();
    }
    // use the values
    analogWrite(CPU, incoming[0]);
    analogWrite(NETWORK, incoming[1]);
    }
}
